import ipfshttpclient
from ipfshttpclient import client


# Monkey-patch assert_version
def assert_v(version: str, minimum: str = '0.4.23',
             maximum: str = '0.12.0') -> None:
    pass


client.assert_version = assert_v


ipfs_client = None


def ipfs_client_get():
    global ipfs_client
    return ipfs_client


def ipfs_client_set_maddr(maddr: str):
    global ipfs_client

    try:
        cli = ipfshttpclient.connect(maddr)
    except Exception:
        ipfs_client = None
    else:
        ipfs_client = cli
